use rcon::Connection;


fn main() {
    let mut connection = Connection::new(&"127.0.0.1:25575".parse().unwrap(), "password123").unwrap();
    let mut input = String::new();
    while let Ok(n) = std::io::stdin().read_line(&mut input) {
        if n == 0 {
            break;
        }
        let response = connection.request(input.trim()).unwrap();
        println!("{}", response);
        input.clear();
    }

}
