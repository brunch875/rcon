//! Basic implementation of a client for the [Source RCON Protocol]
//!
//! [Source RCON Protocol]: https://developer.valvesoftware.com/wiki/Source_RCON_Protocol

use std::convert::TryInto;
use std::io;
use std::io::{Read, Write};
use std::net::{SocketAddr, TcpStream};
use std::time::{Duration, Instant};

mod data;
use data::{
    header_value, AuthRequest, AuthResponse, Command, CommandResponse, Datagram, MalformedDatagram,
    Request, Response, MAX_PACKET_SIZE, SIZE_HEADER,
};

const TIMEOUT: Duration = Duration::from_secs(5);

/// Active RCON connection to a server.
pub struct Connection {
    channel: TcpStream,
    next_packet_id: i32,
}

impl Connection {
    /// Creates a new rcon connection to a remote server indicated by `addr`
    ///
    /// If the connection couldn't be established within 5 seconds, times out
    pub fn new(addr: &SocketAddr, password: &str) -> Result<Self, Error> {
        let channel = TcpStream::connect_timeout(addr, TIMEOUT)?;
        channel.set_write_timeout(Some(TIMEOUT))?;
        let mut connection = Connection {
            channel,
            next_packet_id: 0,
        };
        connection.authenticate(password)?;
        Ok(connection)
    }

    fn authenticate(&mut self, password: &str) -> Result<(), Error> {
        let password = password.to_owned();
        let _: AuthResponse = self.exchange_datagrams(AuthRequest { password })?;
        Ok(())
    }

    /// Sends an rcon command, returning the server response
    /// Times out and returns error if the operation takes longer than 5 seconds
    ///
    /// # Arguments
    ///
    /// * request: console command to send through rcon
    pub fn request(&mut self, request: &str) -> Result<String, Error> {
        let request = Command {
            body: request.into(),
        };
        let response: CommandResponse = self.exchange_datagrams(request)?;
        Ok(response.body)
    }

    /// Sends a request of type Rq to get a response of type Rs. Timeout 5s
    fn exchange_datagrams<Rq, Rs>(&mut self, request: Rq) -> Result<Rs, Error>
    where
        Rq: Request,
        Rs: Response,
    {
        let request: Datagram = Datagram::new(self.next_packet_id, Rq::PACKET_TYPE, request.body())
            .map_err(|_| Error::MessageTooLong)?;
        self.next_packet_id += 1;
        self.channel.write_all(request.payload())?;
        let reply: Datagram = self.read_until_timeout()?;
        if request.id() != reply.id() {
            return Err(Error::IdMismatch);
        }
        reply.try_to_response().map_err(|_| Error::Malformed)
    }

    fn read_until_timeout(&mut self) -> Result<Datagram, Error> {
        let mut buffer = [0u8; MAX_PACKET_SIZE];
        let start = Instant::now();

        let header = &mut buffer[SIZE_HEADER];
        self.channel.set_read_timeout(Some(TIMEOUT)).unwrap();
        self.channel.read_exact(header)?;
        let size: usize = header_value(&buffer)
            .try_into()
            .map_err(|_| Error::Malformed)?;

        let body_start = SIZE_HEADER.end;
        let body_end = body_start + size;
        let body = &mut buffer[body_start..body_end];
        self.channel
            .set_read_timeout(Some(TIMEOUT - start.elapsed()))?;
        self.channel.read_exact(body)?;

        Ok(Datagram::from_payload(buffer)?)
    }
}

#[derive(Debug)]
pub enum Error {
    IdMismatch,
    Malformed,
    TimedOut,
    MessageTooLong,
    IO(io::Error),
}

impl From<io::Error> for Error {
    fn from(error: io::Error) -> Self {
        match error.kind() {
            io::ErrorKind::WouldBlock | io::ErrorKind::TimedOut => Error::TimedOut,
            _ => Error::IO(error),
        }
    }
}

impl From<MalformedDatagram> for Error {
    fn from(_error: MalformedDatagram) -> Self {
        Error::Malformed
    }
}
