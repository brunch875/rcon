pub trait TryFromRawBody {
    type Error;

    fn try_from_raw_body(raw_body: RawBody<'_>) -> Result<Self, Self::Error>
    where
        Self: Sized;
}

pub type PacketType = i32;

pub trait Packet {
    const PACKET_TYPE: PacketType;

    fn body(&self) -> &[u8];
}

type RawBody<'a> = &'a [u8];
pub trait Request: Packet {}

pub trait Response: Packet {}

pub struct AuthRequest {
    pub password: String,
}

pub struct NonEmptyBody;
impl TryFromRawBody for AuthResponse {
    type Error = NonEmptyBody;
    fn try_from_raw_body(body: RawBody<'_>) -> Result<Self, Self::Error> {
        if body.is_empty() {
            Ok(AuthResponse {})
        } else {
            Err(NonEmptyBody {})
        }
    }
}

impl TryFromRawBody for CommandResponse {
    type Error = std::str::Utf8Error;
    fn try_from_raw_body(raw_body: RawBody<'_>) -> Result<Self, Self::Error> {
        Ok(CommandResponse {
            body: std::str::from_utf8(raw_body)?.to_owned(),
        })
    }
}

impl Packet for AuthRequest {
    const PACKET_TYPE: PacketType = 3;

    fn body(&self) -> &[u8] {
        self.password.as_ref()
    }
}

impl Request for AuthRequest {}

pub struct CommandResponse {
    pub body: String,
}

impl Packet for CommandResponse {
    const PACKET_TYPE: PacketType = 0;

    fn body(&self) -> &[u8] {
        self.body.as_ref()
    }
}

impl Response for CommandResponse {}

pub struct AuthResponse {}

impl Packet for AuthResponse {
    const PACKET_TYPE: PacketType = 2;

    fn body(&self) -> &[u8] {
        &[]
    }
}

impl Response for AuthResponse {}

pub struct Command {
    pub body: String,
}

impl Packet for Command {
    const PACKET_TYPE: PacketType = 2;

    fn body(&self) -> &[u8] {
        self.body.as_ref()
    }
}

impl Request for Command {}
