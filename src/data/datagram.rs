use std::convert::TryInto;
use std::ops::Range;

/// Maximum possible packet size including the header
pub const MAX_PACKET_SIZE: usize = 4096;

/// Minimum possible packet size including the header
pub const MIN_PACKET_SIZE: usize = 3 * HEADER_LEN + 1 + 1;

/// Size in bytes of the header specifying the size of the rest of the body
const HEADER_LEN: usize = 4;
pub const SIZE_HEADER: Range<usize> = 0..HEADER_LEN;
const ID_HEADER: Range<usize> = SIZE_HEADER.end..SIZE_HEADER.end + HEADER_LEN;
const TYPE_HEADER: Range<usize> = ID_HEADER.end..ID_HEADER.end + HEADER_LEN;
pub const BODY_START: usize = TYPE_HEADER.end;

pub fn header_value(input: &[u8]) -> i32 {
    let (bytes, _rest) = input.split_at(HEADER_LEN);
    i32::from_le_bytes(bytes.try_into().unwrap())
}

/// Rcon message
pub struct Datagram {
    /// Originating byte array for this datagram.
    /// This is not to be accessed as it's just the memory region
    /// where the body lives.
    payload: [u8; MAX_PACKET_SIZE],
    /// Packet identifier
    id: i32,
    /// Encoded type of the packet
    packet_type: i32,
    /// Range to the actual body of payload
    body_range: Range<usize>,
}

impl std::fmt::Debug for Datagram {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "Size: {}", header_value(&self.payload[0..4]))?;
        writeln!(f, "ID: {}", header_value(&self.payload[4..8]))?;
        writeln!(f, "Type: {}", header_value(&self.payload[8..12]))?;
        write!(f, "Body: ")?;
        for i in 12..30 {
            write!(f, "{};", self.payload[i] as char)?;
        }
        writeln!(f, "...")
    }
}

pub enum MalformedDatagram {
    Size,
    Unterminated,
    NegativeIdentifier,
}

impl Datagram {
    pub fn from_payload(payload: [u8; MAX_PACKET_SIZE]) -> Result<Datagram, MalformedDatagram> {
        let size: usize = header_value(&payload[SIZE_HEADER])
            .try_into()
            .map_err(|_| MalformedDatagram::Size)?;
        if size + HEADER_LEN > MAX_PACKET_SIZE || size + HEADER_LEN < MIN_PACKET_SIZE {
            return Err(MalformedDatagram::Size);
        }

        let id = header_value(&payload[ID_HEADER]);
        if id < 0 {
            return Err(MalformedDatagram::NegativeIdentifier);
        }

        let packet_type = header_value(&payload[TYPE_HEADER]);

        // Those -1 aren't an indexing mistake, we're removing the trailing NULLs
        let packet_end = HEADER_LEN + size - 1;
        let body_end = packet_end - 1;

        if payload[packet_end] != 0x00 || payload[body_end] != 0x00 {
            return Err(MalformedDatagram::Unterminated);
        }

        Ok(Datagram {
            payload,
            id,
            packet_type,
            body_range: BODY_START..body_end,
        })
    }

    /// Creates a new datagram from a body slice and an identifier
    ///
    /// On creation, fills the fields including headers and trailing nullchars
    ///
    /// # Returns
    ///
    /// A result with the created datagram, or error in case the requested
    /// message is too long
    pub fn new(id: i32, packet_type: i32, body: &[u8]) -> Result<Datagram, ()> {
        let mut payload = [0u8; MAX_PACKET_SIZE];

        let size = body.len();
        if size + MIN_PACKET_SIZE > MAX_PACKET_SIZE {
            return Err(());
        }
        let size_in_header = (size + MIN_PACKET_SIZE - HEADER_LEN) as i32;
        payload[SIZE_HEADER].copy_from_slice(&size_in_header.to_le_bytes());
        payload[ID_HEADER].copy_from_slice(&id.to_le_bytes());
        payload[TYPE_HEADER].copy_from_slice(&packet_type.to_le_bytes());

        let body_end = BODY_START + size;
        payload[BODY_START..body_end].copy_from_slice(body);
        payload[body_end] = 0x00;
        payload[body_end + 1] = 0x00;
        Ok(Datagram {
            payload,
            id,
            packet_type,
            body_range: BODY_START..body_end,
        })
    }

    pub fn payload(&self) -> &[u8] {
        &self.payload[..self.body_range.end + 2]
    }

    pub fn id(&self) -> i32 {
        self.id
    }

    pub fn packet_type(&self) -> i32 {
        self.packet_type
    }

    pub fn body(&self) -> &[u8] {
        let start = self.body_range.start;
        let end = self.body_range.end;
        &self.payload[start..end]
    }
}

pub trait TryFromDatagram {
    type Error;

    fn try_from_datagram(datagram: &Datagram) -> Result<Self, Self::Error>
    where
        Self: Sized;
}
