mod datagram;
mod packet;

pub use datagram::{
    header_value, Datagram, MalformedDatagram, TryFromDatagram, MAX_PACKET_SIZE, SIZE_HEADER,
};
pub use packet::{AuthRequest, AuthResponse, Command, CommandResponse, Request};
use packet::{Packet, TryFromRawBody};

pub enum DatagramConversionError<R> {
    RawBody(R),
    TypeMismatch,
}

impl<T> TryFromDatagram for T
where
    T: TryFromRawBody + Packet,
{
    type Error = DatagramConversionError<T::Error>;
    fn try_from_datagram(datagram: &Datagram) -> Result<Self, Self::Error> {
        let expected = Self::PACKET_TYPE;
        let obtained = datagram.packet_type();
        if expected != obtained {
            return Err(DatagramConversionError::TypeMismatch);
        }
        Self::try_from_raw_body(datagram.body()).map_err(DatagramConversionError::RawBody)
    }
}

impl Datagram {
    pub fn try_to_response<R>(&self) -> Result<R, R::Error>
    where
        R: TryFromDatagram,
    {
        R::try_from_datagram(self)
    }
}

// Re-export Response, adding TryFromDatagram as a subtrait
pub trait Response: TryFromDatagram {}
impl<T> Response for T where T: packet::Response + TryFromDatagram {}
